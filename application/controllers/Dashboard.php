<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title']       = 'testing';
		$type = "success";
        $message = "<b>Satrio Dwi Prabowo</b> Was Here ! ";
        set_message($type, $message);
		$data['subview'] = $this->load->view('Dashboard', $data, true);
        $this->load->view('_layout_index', $data);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */